import { BookDAO } from "../src/daos/book-dao";
import { BookDaoPostgres } from "../src/daos/book-dao-postgres";
import { BookDaoTextFile } from "../src/daos/book-dao-textfile-impl";
import { Book } from "../src/entities";

const bookDAO:BookDAO = new BookDaoPostgres();

// Any entity object that has NOT been saved somewhere should have the ID of 0
// This is a standard software convention
const testBook:Book = new Book(0, 'The Hobbit', 'Tolkien', true, 1, 0);

test("Create a book", async ()=>{
    const result:Book = await bookDAO.createBook(testBook);
    console.log(result);
    expect(result.bookID).not.toBe(0); // An entity that is saved should have non-zero ID
});

// An integration test requires that two or more functions you wrote pass
test("Get book by ID", async ()=>{
    let book:Book = new Book(0, "Dracula", "Bram Stoker",true,1,0);
    book = await bookDAO.createBook(book);

    let retrievedBook:Book = await bookDAO.getBookByID(book.bookID); // avoid hard coded id values
    // difficult to maintain

    expect(retrievedBook.title).toBe(book.title);

});

test("Get all books", async () => {
    let book1:Book = new Book(0, 'Sapiens', 'Yuval', true, 0, 0);
    let book2:Book = new Book(0, '1984', 'George Orwell', true,1,0);
    let book3:Book = new Book(0, 'Paradox of Choice', 'Barry Swartz',true,0,0)
    await bookDAO.createBook(book1);
    await bookDAO.createBook(book2);
    await bookDAO.createBook(book3);
    const books:Book[] = await bookDAO.getAllBooks();

    expect(books.length).toBeGreaterThanOrEqual(3);
})

test("Update book", async ()=> {
    let book:Book = new Book(0,'We have always lived in the castle','Shirely Jackson',true,0,0)
    book = await bookDAO.createBook(book);

    // To update we just edit it and then pass it into a method
    book.quality = 4;
    book = await bookDAO.updateBook(book);

    expect(book.quality).toBe(4);
});


test("Delete book by ID", async () => {
    let book:Book = new Book(0, 'Frakenstein', 'Mary Shelley', true, 1, 0);
    book = await bookDAO.createBook(book);

    const result:boolean = await bookDAO.deleteBookByID(book.bookID);
    expect(result).toBeTruthy();

});

// Try to avoid methods in your program that return void
// Methods that do not return data are VERY difficult to test
// For the most part you want your methods to take in objects 