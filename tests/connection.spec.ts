import { client } from '../src/connection';

// client is the main object we will use to interact w/ our database

test("Should create a connection ", async ()=> {
    
    const result = await client.query('select * from book'); // we need await in 
    // order to get the information from our database    

    console.log(result);

});