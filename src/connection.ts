import {Client} from 'pg';
require('dotenv').config({path:'D:\\Revature\\defaultCodeRepository\\library-practice-repo\\app.env'});
export const client = new Client({
    user:'postgres',
    password:process.env.DBPASSLIB, // YOU SHOULD NEVER STORE PASSWORDS IN CODE
    database:process.env.DBNAMELIB,
    port:5432,
    host:'35.245.7.192'
})
client.connect()