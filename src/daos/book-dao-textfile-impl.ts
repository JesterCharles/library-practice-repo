import { BookDAO } from "../../src/daos/book-dao";
import { Book } from "../entities";
import {readFile, writeFile} from 'fs/promises'
import { MissingResourceError } from "../errors";

export class BookDaoTextFile implements BookDAO{
    
    async createBook(book: Book): Promise<Book> {
        const fileData:Buffer = await readFile('D:\\Revature\\defaultCodeRepository\\week2\\day1\\libraryAPI\\books.txt');
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // Take the JSON text and turn it into an object
        book.bookID = Math.round(Math.random()*1000); // Random number ID for the book
        books.push(book); // add out book to the array
        await writeFile('D:\\Revature\\defaultCodeRepository\\week2\\day1\\libraryAPI\\books.txt', JSON.stringify(books));
        return book;

    }

    async getAllBooks(): Promise<Book[]> {
        const fileData:Buffer = await readFile('D:\\Revature\\defaultCodeRepository\\week2\\day1\\libraryAPI\\books.txt');
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // Take the JSON text and turn it into an object
    
        return books;
    }

    async getBookByID(bookID: number): Promise<Book> {
        const fileData:Buffer = await readFile('D:\\Revature\\defaultCodeRepository\\week2\\day1\\libraryAPI\\books.txt');
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // Take the JSON text and turn it into an object
    
        for(const book of books){
            if(book.bookID === bookID){
                return book;
            }
        }
        throw new MissingResourceError(`the book with ID ${bookID} could not be located`);
    }

    async updateBook(book: Book): Promise<Book> {
        const fileData:Buffer = await readFile('D:\\Revature\\defaultCodeRepository\\week2\\day1\\libraryAPI\\books.txt');
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // Take the JSON text and turn it into an object
        
        for(let i = 0; i < books.length; i++){
            if(books[i].bookID === book.bookID){
                books[i] = book;
            }

        }
        
        await writeFile('D:\\Revature\\defaultCodeRepository\\week2\\day1\\libraryAPI\\books.txt', JSON.stringify(books));
        return book;
    }

    // Async functions MUST ALWAYS return a promise
    async deleteBookByID(bookID: number): Promise<boolean> {
        const fileData:Buffer = await readFile('D:\\Revature\\defaultCodeRepository\\week2\\day1\\libraryAPI\\books.txt');
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // Take the JSON text and turn it into an object
        
        for(let i = 0; i < books.length; i++){
            if(books[i].bookID === bookID){
                books.splice(i); //remove that book
                await writeFile('D:\\Revature\\defaultCodeRepository\\week2\\day1\\libraryAPI\\books.txt', JSON.stringify(books));
                return true;
            }
        }
        return false;
    }
}