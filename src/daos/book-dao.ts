// DAO (Data Access Object)
// A class that is responsible for persisting an entity.
// Very much linked to the data layer. Should support the CRUD functionalities.

import { Book } from "../entities";

export interface BookDAO{
    // CREATE
    createBook(book:Book):Promise<Book>; // return a promise that'll eventually be a book
    
    // READ
    getAllBooks():Promise<Book[]>;
    getBookByID(bookID:number):Promise<Book>;
    
    // UPDATE
    updateBook(book:Book):Promise<Book>;

    // DELETE
    deleteBookByID(bookID:number):Promise<boolean>;

}