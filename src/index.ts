import express from 'express';
import { Book } from './entities';
import { MissingResourceError } from './errors';
import BookService from './services/book-service';
import { BookServiceImpl } from './services/book-service-impl';

const app = express();
app.use(express.json()); // Middleware

// Our application routes should use services we create to do the heavy lifting
// Try to minimize the amount of logic in your routes that is NOT related
// Directly to HTTP requests and responses

const bookService:BookService = new BookServiceImpl();

app.get("/books", async(req, res) => {
        const books:Book[] = await bookService.retrieveAllBooks();
        res.status(201);
        res.send(books);
});

app.get("/books/:id", async (req,res) => {
try {    
    const bookID = Number(req.params.id);
    const book:Book = await bookService.retrieveBookByID(bookID);
    res.send(book);
} catch (error) {
    if(error instanceof MissingResourceError){
        res.status(404);
        res.send(error);
    }
}
});

app.post("/books", async (req, res) => {
    let book:Book = req.body;
    book = await bookService.registerBook(book);
    res.send(book)
});

app.patch("/books/:id/checkout", async (req, res)=> {
    const bookID:number = Number(req.params.id)
    const book:Book|string = await bookService.checkoutBookByID(bookID);
    res.send(book);
})

app.listen(3000, () => {console.log("Server is now running!\n")});