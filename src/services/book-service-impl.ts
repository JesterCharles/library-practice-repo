
import { BookDAO } from '../daos/book-dao';
import { BookDaoPostgres } from '../daos/book-dao-postgres';
import { BookDaoTextFile } from '../daos/book-dao-textfile-impl';
import { Book } from '../entities';
import BookService from './book-service';

export class BookServiceImpl implements BookService{
    
    bookDAO:BookDAO = new BookDaoPostgres();

    registerBook(book: Book): Promise<Book> {
        book.returnDate = 0; // services are often sanitize inputs or set default values
        if(book.quality < 1 ){
            book.quality = 1;
        }
        return this.bookDAO.createBook(book);
    }

    retrieveAllBooks(): Promise<Book[]> {
        return this.bookDAO.getAllBooks();
    }

    retrieveBookByID(bookID: number): Promise<Book> {
        return this.bookDAO.getBookByID(bookID);
    }

    // Service methods also perform business logic
    async checkoutBookByID(bookID: number): Promise<Book | string>{
        let book:Book = await this.bookDAO.getBookByID(bookID);
        if(book.isAvailable === true){
            book.isAvailable = false;
            book.returnDate = Date.now() + 1_209_600; // you can use underscores in numbers for readability
            this.bookDAO.updateBook(book);
            return book;
        } else {
            const out = "Book has already been taken out!";
            return out;
        }
    }

    checkinBookByID(bookID: number): Promise<Book> {
        throw new Error('Method not implemented.');
    }

    searchByTitle(title: string): Promise<Book> {
        throw new Error('Method not implemented.');
    }

    modifyBook(book: Book): Promise<Book> {
        throw new Error('Method not implemented.');
    }

    removeBookByID(bookID: number): Promise<boolean> {
        throw new Error('Method not implemented.');
    }
}