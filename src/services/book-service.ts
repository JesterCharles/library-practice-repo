import { Book } from "../entities";

// Your service interface should have all the methods that your RESTful web service
// will find helpful.
// 1. Methods that perform CRUD operations
// 2. Business Logic operations


export default interface BookService{

    registerBook(book:Book):Promise<Book>;

    retrieveAllBooks():Promise<Book[]>;

    retrieveBookByID(bookID:number):Promise<Book>;

    checkoutBookByID(bookID:number):Promise<Book | string>;

    checkinBookByID(bookID:number):Promise<Book>

    searchByTitle(title:string):Promise<Book>;

    modifyBook(book:Book):Promise<Book>;

    removeBookByID(bookID:number):Promise<boolean>;

}