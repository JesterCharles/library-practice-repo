// An entity is a class that store information that will 
// ultimately be persisted somewhere
// Usually very little logic
// They SHOULD ALWAYS have one field in them that is a unique indentifier, ID

export class Book{

    constructor(
        public bookID:number,
        public title:string,
        public author:string,
        public isAvailable:boolean,
        public quality:number,
        public returnDate:number // Typicall dates are stored as uniq epoch time
        // second from midnight January 1970
    ) {}

}